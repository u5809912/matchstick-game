import Model.*;


/**
 * Created by nsifniotis on 1/09/15.
 * go away warning
 */
public class MatchstickGame {

    public static void GameLoop ()
    {
        Player[] players = new Player [Constants.NUM_PLAYERS];
        players[0] = new HumanPlayer();
        players[1] = new ComputerPlayer();

        GameState game = new GameState();
        game.scores = new int [Constants.NUM_PLAYERS];
        game.b = new Board (Constants.BOARD_SIZE);
        game.curr_player = 0;
        game.whoami = 1;                // yeah thats the computer player

        while (game.b.gameOn())
        {
            System.out.println ("\n\n" + game.b + "\nPlayer " + game.curr_player + ", your move.");

            Move m = players[game.curr_player].get_valid_move(game);
            game.makeMove(m);
        }

        System.out.println ("Yay - game over! Scores are:");
        for (int s: game.scores)
            System.out.println (s);
    }

    public static void main(String[] args) {
        GameLoop();
    }
}
