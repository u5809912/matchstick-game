package Model;

/**
 * Created by nsifniotis on 1/09/15.
 */
public class Parser {

    private static String game_to_parse;

    public static void reset (String s)
    {
        game_to_parse = s;
    }

    public static Move next_move() throws Exception
    {
        if (game_to_parse.length() < 3)
            throw new Exception ("Parser out of input.");

        boolean finished = false;
        String toke = "";

        while (!finished && game_to_parse.length() > 0)
        {
            char next_char = game_to_parse.toCharArray()[0];
            game_to_parse = game_to_parse.substring(1);

            if (next_char >= 'A' && next_char <= 'Z')
                toke += String.valueOf(next_char);

            finished = (toke.length() == 3);
        }

        if (toke.length() != 3)
            throw new Exception ("Parser out of input again!");

        char [] chars = toke.toCharArray();
        Move res = new Move ();

        res.isRow = (chars[0] == 'R');
        res.rowCol = chars[1] - 'A';
        res.moveNumber = chars[2] - 'A';

        return res;
    }
}
