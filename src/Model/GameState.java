package Model;

/**
 * Created by nsifniotis on 1/09/15.
 *
 *
 */
public class GameState {

    public Board b;
    public int [] scores;
    public int curr_player;
    public int whoami;

    public boolean legitMove (Move m)
    {
        return b.legitMove(m);
    }

    public void makeMove (Move m)
    {
        int s = b.scoreMove(m);
        if (s == 0)
            curr_player = (curr_player + 1) % Constants.NUM_PLAYERS;
        else
            scores[curr_player] += s;
        b.makeMove(m);
    }
}
