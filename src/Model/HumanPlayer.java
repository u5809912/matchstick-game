package Model;

import java.util.Scanner;

/**
 * Created by nsifniotis on 1/09/15.
 */
public class HumanPlayer implements Player {

    @Override
    public Move get_valid_move(GameState gs) {
        boolean got_move = false;
        Move m = new Move();
        Scanner in = new Scanner(System.in);

        while (!got_move)
        {
            System.out.println("\nEnter a valid move");
            Parser.reset(in.nextLine().toUpperCase());

            try
            {
                m = Parser.next_move();
                got_move = gs.b.legitMove(m);
            }
            catch (Exception e)
            {
                System.out.println (e);
            }
        }

        return m;
    }
}
