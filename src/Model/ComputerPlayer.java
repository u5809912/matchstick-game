package Model;

import sun.util.resources.cldr.st.CurrencyNames_st;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by nsifniotis on 1/09/15.
 *
 *
 */
public class ComputerPlayer implements Player{

    @Override
    public Move get_valid_move(GameState gs)
    {
        // clear the stats counters things
        for (int i = 0; i < Constants.depth_counter.length; i ++)
            Constants.depth_counter[i] = 0;


        // build a bunch of game states and maximise the result
        List <MoveGSPair> states = new ArrayList<>();
        GameState newState;
        MoveGSPair pair;

        for (Move m: get_valid_moves(gs.b))
        {
            newState = copy(gs);
            pair = new MoveGSPair();

            newState.makeMove(m);

            pair.gs = newState;
            pair.m = m;

            states.add (pair);
        }

        // get the farking move!

        int depth = 3;
        if (states.size() < 20)
            depth = 4;
        if (states.size() < 16)
            depth = 5;
        if (states.size() < 12)
            depth = 6;
        if (states.size() < 8)
            depth = 8;

        int best = -10;
        Move bester = null;
        for (MoveGSPair p: states)
        {
            int r = minimax (p.gs, depth);
            if (r > best)
            {
                best = r;
                bester = p.m;
            }
        }


        for (int i = 0; i < Constants.depth_counter.length; i ++)
            System.out.println ("Depth " + i + ": " + Constants.depth_counter[i]);

        
        return bester;
    }

    public int minimax (GameState state, int depth)
    {
        if (depth == 0)
            return state.scores[state.whoami] - state.scores[0];

        int ressy = 0;
        List <Move> moves = get_valid_moves(state.b);

        Constants.depth_counter[depth] += moves.size();

        if (moves.size() == 0)
            return state.scores[state.whoami] - state.scores[0];

        List <GameState> states = new ArrayList<>();

        for (Move m: moves)
        {
            GameState newState = copy(state);
            newState.makeMove(m);

            states.add (newState);
        }

        if (state.curr_player == state.whoami)
        {
            // maximise
            int maxy = -10;
            for (GameState gs: states)
            {
                int result = (minimax(gs, depth - 1));
                if (maxy < result)
                    maxy = result;
            }

            ressy = maxy;
        }
        else
        {
            // minimise
            int minny = 1000;
            for (GameState gs: states)
            {
                int result = (minimax(gs, depth - 1));
                if (minny > result)
                    minny = result;
            }

            ressy = minny;
        }

        return ressy;
    }

    private List<Move> get_valid_moves(Board b)
    {
        List <Move> res = new LinkedList<>();
        Move temp;

        for (int i = 0; i < b.board_size; i ++)
            for (int j = 0; j < b.board_size - 1; j ++)
            {
                temp = new Move();
                temp.isRow = true;
                temp.moveNumber = j;
                temp.rowCol = i;

                if (b.legitMove(temp))
                    res.add (temp);

                temp = new Move();
                temp.isRow = false;
                temp.moveNumber = j;
                temp.rowCol = i;

                if (b.legitMove(temp))
                    res.add (temp);
            }
        return res;
    }

    private GameState copy (GameState g)
    {
        GameState res = new GameState();
        res.curr_player = g.curr_player;
        res.whoami = g.whoami;
        res.scores = new int [g.scores.length];
        for (int i = 0; i < g.scores.length; i ++)
            res.scores[i] = g.scores[i];

        res.b = g.b.deep_copy();

        return res;
    }
}
