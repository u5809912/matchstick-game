package Model;

/**
 * Created by nsifniotis on 1/09/15.
 */
public class Board {

    public boolean [][] rows;
    public boolean [][] cols;

    public int board_size;


    public Board (int board_size)
    {
        this.board_size = board_size;
        rows = new boolean [board_size][board_size - 1];
        cols = new boolean [board_size][board_size - 1];
    }

    public Board deep_copy()
    {
        Board res = new Board(this.board_size);

        for (int i = 0; i < board_size; i ++)
            for (int j = 0; j < board_size - 1; j ++)
            {
                res.rows[i][j] = this.rows[i][j];
                res.cols[i][j] = this.cols [i][j];
            }

        return res;
    }

    public void makeMove (Move m)
    {
        if (!legitMove(m))
            return;


        if (m.isRow)
            rows[m.rowCol][m.moveNumber] = true;
        else
            cols[m.rowCol][m.moveNumber] = true;
    }

    public boolean legitMove (Move m)
    {
        if (m.rowCol < 0 || m.rowCol >= this.board_size || m.moveNumber >= board_size - 1)
            return false;


        if (m.isRow)
        {
            if (rows[m.rowCol][m.moveNumber])
                return false;
        }
        else
        {
            if (cols[m.rowCol][m.moveNumber])
                return false;
        }

        return true;
    }

    public int scoreMove (Move m)
    {
        // says nothing about the legality of the move. It is only interested in how many boxes are made
        // it's either 0, 1 or 2.

        if (!legitMove(m))
            return 0;

        int res = 0;

        if (m.isRow)
        {
            if (m.rowCol > 0)
            {
                if (rows[m.rowCol - 1][m.moveNumber] && cols[m.moveNumber][m.rowCol - 1] && cols[m.moveNumber + 1][m.rowCol - 1])
                    res ++;
            }

            if (m.rowCol < this.board_size - 1)
            {
                if (rows[m.rowCol + 1][m.moveNumber] && cols[m.moveNumber][m.rowCol] && cols[m.moveNumber + 1][m.rowCol])
                    res ++;
            }
        }
        else
        {
            // same reasoning but the rows and cols are swapped over
            if (m.rowCol > 0)
            {
                if (cols[m.rowCol - 1][m.moveNumber] && rows[m.moveNumber][m.rowCol - 1] && rows[m.moveNumber + 1][m.rowCol - 1])
                    res ++;
            }

            if (m.rowCol < this.board_size - 1)
            {
                if (cols[m.rowCol + 1][m.moveNumber] && rows[m.moveNumber][m.rowCol] && rows[m.moveNumber + 1][m.rowCol])
                    res ++;
            }
        }

        return res;
    }

    public boolean gameOn()
    {
        boolean res = false;

        for (int i = 0; i < board_size; i ++)
            for (int j = 0; j < board_size - 1; j ++)
                res |= (!rows[i][j]) | (!cols[i][j]);

        return res;
    }

    @Override
    public String toString ()
    {
        String res = "";

        for (int i = 0; i < this.board_size; i ++)
        {
            // first line
            for (int j = 0; j < this.board_size; j ++)
            {
                res += ".";
                if (i < this.board_size && j < this.board_size - 1)
                    if (rows[i][j])
                        res += "-";
                    else
                        res += " ";
            }

            res += "\n";

            // second line
            if (i < board_size - 1)
            {
                for (int j = 0; j < this.board_size; j ++)
                {
                    if (cols[j][i])
                        res += "| ";
                    else
                        res += "  ";
                }

                res += "\n";
            }
        }

        return res;
    }
}
